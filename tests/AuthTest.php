<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
// use PHPUnit\Framework\TestCase;
use App\Models\User;

class AuthTest extends TestCase
{
    /** @test */
    public function login_success()
    {
        $user = User::factory()->create();
        $this->json('post', '/api/auth/login', [
            'email' => $user->email,
            'password' => 'password'
        ])
            ->seeStatusCode(200)
            ->seeJsonStructure([
                'data' => [
                    'user' => [
                        'id',
                        'name',
                        'email',
                        'role',
                        'created_at',
                        'updated_at'
                    ],
                    'error'
                ]
            ]);
    }
    /** @test*/
    public function login_failed()
    {
        $user = User::factory()->create();
        $this->json('POST', '/api/auth/login', [
            'email' => $user->email,
            'password' => 'secret'
        ])
            ->seeStatusCode(401)
            ->seeJson([
                'message' => 'login gagal'
            ])
            ->seeJsonStructure([
                'success',
                'message',
                'data',
                'error'
            ]);
    }
}
