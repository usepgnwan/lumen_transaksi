<?php

namespace Database\Factories;

use App\Models\item;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Arr;

class ItemFactory extends Factory
{
    protected $model = item::class;

    public function definition(): array
    {
        $itemPrefixes = ['Sweater', 'Pants', 'Shirt', 'Hat', 'Glasses', 'Socks'];
        $name = $this->faker->company . ' ' . Arr::random($itemPrefixes);
        return [
            'category_id' => rand(1, 5),
            'name' => $name,
            'barcode' => $this->faker->ean13,
            'slug' => $name,
            'description' => $this->faker->realText(191),
            'price' => $this->faker->numberBetween(10000, 100000),
            'quantity' => rand(1, 15),
        ];
    }
}
