<?php

namespace Database\Factories;

use App\Models\Table;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class TableFactory extends Factory
{
    protected $model = Table::class;

    private static $i = 1;
    public function definition(): array
    {
        $name = 'Meja ' . self::$i++;
        return [
            'name' => $name,
            'slug' => Str::slug($name)
        ];
    }
}
