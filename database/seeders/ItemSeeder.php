<?php

namespace Database\Seeders;

use App\Models\Item;
use Faker\Factory;
use Illuminate\Database\Seeder;

class ItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $count = $this->command->ask("mau berapa nich?");
        $check = $this->command->confirm('Yakin?');
        $item = new Item;
        if ($check) {
            $item::factory()->count($count)->create();

            $item::all()->each(function ($itm) use ($faker) {
                $itm->image()->create([
                    'url' => $faker->imageUrl(360, 360, 'animals', true, 'cats')
                ]);
            });
            $this->command->info('berhasil simpan ' . $count);
        } else {
            $this->command->info('gagal');
        }
    }
}
