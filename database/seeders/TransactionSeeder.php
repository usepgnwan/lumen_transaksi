<?php

namespace Database\Seeders;

use App\Models\Item;
use App\Models\Transaction;
use Faker\Factory;
use Illuminate\Database\Seeder;

class TransactionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $trans = new Transaction;
        $faker = Factory::create();
        $count =  $this->command->ask('mau berapa transaksi?');
        $trans->factory()->count($count)->create();
        $this->command->info('Berhasil');

        $trans::all()->each(function ($transaction) use ($faker) {
            $item_id = rand(1, 6);
            $item = Item::find($item_id);
            $quantity = rand(1, 5);
            $price = $item->price;
            $subtotal = $quantity * $price;
            $details[] = [
                'item_id' => $item->id,
                'quantity' => $quantity,
                'price' => $price,
                'subtotal' => $subtotal
            ];
            $transaction->details()->createMany($details);
        });
    }
}
