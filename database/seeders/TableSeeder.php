<?php

namespace Database\Seeders;

use App\Models\Table;
use Illuminate\Database\Seeder;

class TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = $this->command->ask('Mau bikin berap?', 5);

        $check =  $this->command->confirm("Yakin Simpen?");
        $table = new Table;
        if ($check) {
            $table::factory()->count($count)->create();
            $this->command->info('Berhasil Menyimpan ' . $count);
        } else {
            $this->command->info('Gagal');
        }
    }
}
