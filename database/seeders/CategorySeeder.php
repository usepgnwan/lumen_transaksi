<?php

namespace Database\Seeders;

use App\Models\Category;
use Faker\Factory as Factory;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $category = new Category();
        $count = (int)$this->command->ask("mau berapa kategori", 5);

        $check = $this->command->confirm("yakin buat kategory : " . $count);
        if ($check) {
            $category::factory()->count($count)->create();

            $category::all()->each(function ($category) use ($faker) {
                $category->image()->create([
                    'url' => $faker->imageUrl(360, 360, 'animals', true, 'cats')
                ]);
            });
            $this->command->info('kategorinya : ' . $count);
        } else {
            $this->command->info('kategorinya Batal dibuat');
        }
    }
}
