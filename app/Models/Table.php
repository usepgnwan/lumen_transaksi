<?php

namespace App\Models;

use App\Models\Concerns\Searchable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Table extends Model
{
    use HasFactory, Searchable;
    protected $guarded = [
        'id'
    ];
    public function setSlugAttribute($val)
    {
        $this->attributes['slug'] = Str::slug($val);
    }

    public function transaction()
    {
        $this->hasMany(Transaction::class);
    }
}
