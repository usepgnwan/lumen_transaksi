<?php

namespace App\Models\Concerns;

use App\Models\Transaction;

trait InvoiceTrait
{
    public static function boot()
    {
        parent::boot();

        static::create(function ($invoice) {
            $curen_prefix = 'INV';
            $record = Transaction::orderBy('id', 'desc')->first();
            $nextInvoice = 1;
            if ($record) {
                $nextInvoice = ($record->id + 1);
            }

            $invoice->invoice = $curen_prefix . '-' . str_pad(
                $nextInvoice,
                5,
                0,
                STR_PAD_LEFT
            );
        });
    }
}
