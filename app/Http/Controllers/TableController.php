<?php

namespace App\Http\Controllers;

use App\Models\Table;
use App\Utils\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class TableController extends Controller
{
    protected $tb;
    public function __construct(Table $table)
    {
        $this->tb = $table;
    }
    public function index()
    {
        $data = $this->tb->latest();
        $keyword = request()->keyword;
        if ($keyword) {
            $data->search($keyword);
        }
        $data = $data->paginate(10);
        return response()->json(new JsonResponse('Data Semua Table', $data), Response::HTTP_OK);
    }
    public function store(Request $request)
    {
        $data = Validator::make($request->all(), [
            'name' => 'required|max:255|unique:tables'
        ]);

        if ($data->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'Gagal simpan table',
                'data' => $data->errors(),
                'error' => 'error_create'
            ], Response::HTTP_BAD_REQUEST);
        }
        $data = [
            'name' => $request->name,
            'slug' => $request->name
        ];
        $store = $this->tb->create($data);

        return response()->json(new JsonResponse('Berhasil simpan baru', $store), Response::HTTP_OK);
    }
    public function shows(int $id)
    {
        $data = $this->tb->find($id);

        if (!$data) {
            return response()->json(new JsonResponse('Data Tidak Ditemukan', $data, 'cannot_find'), Response::HTTP_NOT_FOUND);
        }

        return response()->json(new JsonResponse('Data Table Ditemukan', $data), Response::HTTP_NOT_FOUND);
    }

    public function destroy(int $id)
    {
        try {
            $data = $this->tb->find($id);

            if (!$data) {
                return response()->json(new JsonResponse('Data Tidak Ditemukan', $data, 'cannot_find'), Response::HTTP_NOT_FOUND);
            }
            $data->delete();
            return response()->json(new JsonResponse('Berhasil Hapus Table', []), Response::HTTP_NOT_FOUND);
        } catch (\Exception $e) {
            return response()->json(new JsonResponse('Data Tidak Ditemukan', ["msg" => $e->getMessage()], 'cannot_find'), Response::HTTP_NOT_FOUND);
        }
    }

    public function update(Request $request, int $id)
    {
        try {
            $find = $this->tb->find($id);
            $valid = Validator::make($request->all(), [
                'name' => 'required|max:255|unique:tables'
            ]);
            if (!$find) {
                return response()->json(new JsonResponse('Data Tidak Ditemukan', $find, 'cannot_find'), Response::HTTP_NOT_FOUND);
            }

            $data = [
                "name" => $request->name,
                "slug" => $request->name,
            ];

            $find->fill($data);
            $find->save();
            return response()->json(new JsonResponse('Data Tidak Ditemukan', $find, 'cannot_find'), Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json(new JsonResponse(
                'Perubahan Meja Gagal',
                ['data' => $e->getMessage()],
                'update_error'
            ), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
