<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use App\Models\Todo;
use Exception;
use Illuminate\Http\{Request, Response};

class TodoController extends Controller
{
    protected $todo;
    public function __construct(Todo $todo)
    {
        //  parent::__construct();
        $this->todo = $todo;
    }

    public function index()
    {
        $data = $this->todo->get();
        return response()->json(['status' => true, 'msg' => 'sukses', 'data' => $data], 200);
    }

    public function shows($id)
    {

        $data = $this->todo->find($id);
        if ($data) {
            return response()->json(['status' => true, 'msg' => 'success', "data" => $data], 200);
        } else {
            return response()->json(['status' => false, 'msg' => 'Tidak Ditemukan'], 404);
        }
    }
    public function store(Request $request)
    {
        $data = $this->validate_form($request);

        if ($data->fails()) {
            return response()->json(['status' => false, "msg" =>  $data->errors()], 404);
        } else {
            $datas = $this->todo->create([
                'name' => $request->input('name'),
                'description' => $request->input('description'),
            ]);
            return response()->json(['status' => true, "msg" => 'berhasil insert data'], 200);
        }
    }
    public function update(Request $request, $id)
    {
        $valid =  $this->validate_form($request);
        if ($valid->fails()) {
            return response()->json(['status' => false, "msg" =>  $valid->errors()], 404);
        } else {
            $check = $this->todo->find($id);
            if ($check) {
                $check->update([
                    "name" => $request->input('name'),
                    "description" => $request->input('description'),
                ]);
                $check->save();
                return response()->json(['status' => true, "msg" => 'berhasil edit data'], 200);
            } else {
                return response()->json(['status' => false, "msg" => "gagal update data"], 404);
            }
        }
    }

    public function destroy($id)
    {
        $check = $this->todo->find($id);
        if ($check) {
            $check->delete();
            return response()->json(['status' => true, "msg" => 'berhasil hapus data'], 200);
        } else {
            return response()->json(['status' => false, "msg" => "data tidak ditemukan"], 404);
        }
    }
    protected function validate_form($data)
    {
        $valid =  Validator::make($data->all(), [
            'name' => 'required|max:100',
            'description' => 'required'
        ]);
        return $valid;
    }
}
