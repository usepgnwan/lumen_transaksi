<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Utils\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    protected $repo;
    public function __construct(Category $repo)
    {
        $this->repo = $repo;
    }

    public function index()
    {
        $categories = $this->repo
            ->with('image')
            ->latest();

        $keyword = request()->keyword;

        // echo ($keyword);
        // die;
        if ($keyword) {
            $categories = $categories->search($keyword);
        }
        $categories = $categories->paginate(10);
        return response()->json(new JsonResponse('data semua category', $categories), Response::HTTP_OK);
    }

    public function store(Request $request)
    {
        $data =  Validator::make($request->all(), [
            'name' => 'required|max:255|unique:categories',
            'image' => 'required|image|mimes:png,jpg,jpeg|max:10000'
        ]);

        try {
            if ($data->fails()) {
                return response()->json(new JsonResponse(
                    'Gagal Menyimpan data',
                    $data->errors()
                ), Response::HTTP_OK);
            } else {
                $data = [
                    'name' => $request->name,
                    'slug' => $request->name
                ];
                $store = $this->repo->create($data);
                if ($request->hasFile('image') || $request->image != '') {
                    $image = $request->file('image');
                    $imageName = $image->hashName();
                    $store->image()->create(
                        ['url' => $imageName]
                    );
                    $image->move(storage_path('images'), $imageName);
                }
                return response()->json(new JsonResponse('sukses menyimpan data', $store), Response::HTTP_OK);
            }
        } catch (\Exception $e) {
            return response()->json(new JsonResponse('gagal menyimpan', $e->getMessage(), 'error'), Response::HTTP_UNAUTHORIZED);
        }
    }

    public function shows(int $id)
    {
        $categories = $this->repo->with('image')->find($id);

        if (!$categories) {
            return response()->json(new JsonResponse('Data tidak ditemukan', [], 'show_not_found'), Response::HTTP_NOT_FOUND);
        }
        return response()->json(new JsonResponse('Data berhasil ditemukan', $categories), Response::HTTP_OK);
    }

    public function update(Request $request, int $id)
    {
        $categories = $this->repo->find($id);
        if (!$categories) {
            return response()->json(new JsonResponse('Data tidak ditemukan', [], 'show_not_found'), Response::HTTP_OK);
        }

        $data = Validator::make($request->all(), [
            'name' => 'required|max:225',
            'image' => 'required|image|mimes:png,jpg,jpeg|max:10000'
        ]);
        if ($data->fails()) {
            return response()->json(new JsonResponse('Lengkapi Form Data', $data->errors(), 'error'), Response::HTTP_OK);
        }

        try {
            $data = [
                'name' => $request->name,
                'slug' => $request->name,
            ];
            $categories->fill($data);
            $categories->save();
            if ($request->hasFile('image') || $request->image != '') {
                $image = $request->file('image');
                $imageName = $image->hashName();
                $linkPath = storage_path('images') . '/' . $categories->image->url;
                unlink($linkPath);
                $categories->image()->update(
                    ['url' => $imageName]
                );
                $image->move(storage_path('images'), $imageName);
            }
            return response()->json(new JsonResponse('Berhasil Update Category', $categories), Response::HTTP_OK);
        } catch (\Exception $e) {
            //return error message
            return response()->json(new JsonResponse(
                'Perubahan Kategori Gagal',
                ['data' => $e->getMessage()],
                'update_error'
            ), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function destroy(int $id)
    {
        $categories = $this->repo->find($id);

        if (!$categories) {
            return response()->json(new JsonResponse('Not Found', [], 'categories_not_found'), Response::HTTP_NOT_FOUND);
        }

        try {
            if ($categories->delete()) {
                $fileName = $categories->image->url;
                $path =  storage_path('images') . '/' . $fileName;
                unlink($path);
                $categories->image->delete();
            }
            $categories->delete();
            return response()->json(new JsonResponse('Berhasil Hapus Data Categories', []), Response::HTTP_NOT_FOUND);
        } catch (\Exception $e) {
            return response()->json(new JsonResponse('Error', ["msg" => $e->getMessage()], 'delete_error'), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
