<?php

namespace App\Http\Controllers;

use App\Utils\JsonResponse;
use Exception;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth:api', ['except' => ['login', 'register']]);
    // }
    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function login()
    {

        try {

            $credentials = request(['email', 'password']);


            if (!$token = auth()->attempt($credentials)) {
                return response()->json(new JsonResponse(
                    'Login Gagal',
                    [],
                    'login_error'
                ), Response::HTTP_UNAUTHORIZED);
            }

            return response()->json(new JsonResponse(
                'Login Berhasil',
                [
                    'user' => auth()->user(),
                    'access_token' => $token,
                    'token_type' => 'bearer',
                    'expires_in' => auth()->factory()->getTTL() * 3600
                ]
            ), Response::HTTP_OK);
        } catch (Exception $e) {
            dd($e);
        }
    }
    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function me()
    {
        return response()->json(new JsonResponse(
            'Me',
            auth()->user()
        ), Response::HTTP_OK);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();
        return response(new JsonResponse(
            'Berhasil Logout',
            []
        ), Response::HTTP_OK)->json();
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return response()->json(new JsonResponse(
            'Refresh Token',
            auth()->refresh()
        ), Response::HTTP_OK);
    }
}
