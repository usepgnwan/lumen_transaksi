<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Utils\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class ItemController extends Controller
{
    protected $items;
    public function __construct(Item $item)
    {
        $this->items = $item;
    }

    public function index()
    {
        $data = $this->items->with('image')->latest();

        $keyword = request()->keyword;

        // echo ($keyword);
        // die;
        if ($keyword) {
            $data = $data->search($keyword);
        }
        $data = $data->paginate(4);

        return response()->json(new JsonResponse('Sukses', $data), Response::HTTP_OK);
    }

    public function shows(int $id)
    {
        $data = $this->items->with('image')->find($id);
        if (!$data) {
            return response()->json(new JsonResponse('Gagal data tidak ditemukan', $data, 'error'), Response::HTTP_NOT_FOUND);
        }

        return response()->json(new JsonResponse('Berhasil Menemukan data', $data), Response::HTTP_OK);
    }

    public function create(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'category_id' => 'required|exists:categories,id',
            'barcode' => 'required|integer|unique:items',
            'name' => 'required|unique:items',
            'description' => 'required',
            'price' => 'required|integer',
            'quantity' => 'required|integer',
            'image' => 'required|image|mimes:jpeg,png,jpg,JPG,JPEG,PNG|max:10000',
        ]);
        try {

            if ($validate->fails()) {
                return response()->json([
                    'success' =>  false,
                    'message' => 'Gagal Lengkapi Data',
                    'data' => $validate->errors(),
                    'error' => 'error'
                ], Response::HTTP_BAD_REQUEST);
            }

            $data = [
                'category_id' => $request->category_id,
                'barcode' => $request->barcode,
                'name' => $request->name,
                'slug' => $request->name,
                'description' => $request->description,
                'price' => $request->price,
                'quantity' => $request->quantity,
            ];
            $store = $this->items->create($data);

            if ($request->hasFile('image') && $request->image != '') {
                $img = $request->file('image');
                $imgName = $img->hashName();
                $store->image()->create([
                    'url' => $imgName
                ]);
                $img->move(storage_path('images'), $imgName);
            }
            return response()->json(new JsonResponse('Sukses', $store), Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json(new JsonResponse(
                'Pembuatan Item Gagal',
                [
                    $e->getMessage(),
                    'create_error'
                ],
                'create_error'
            ), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function update(Request $request, $id)
    {
        $validate = Validator::make($request->all(), [
            'category_id' => 'required|exists:categories,id',
            'barcode' => 'required|integer|unique:items',
            'name' => 'required|unique:items',
            'description' => 'required',
            'price' => 'required|integer',
            'quantity' => 'required|integer',
            // 'image' => 'required|image|mimes:jpeg,png,jpg,JPG,JPEG,PNG|max:10000',
        ]);

        try {
            $check = $this->items->find($id);
            if (!$check) {
                return response()->json([
                    'success' =>  false,
                    'message' => 'Id tidak ditemukan',
                    'data' =>  $check,
                    'error' => 'error'
                ], Response::HTTP_BAD_REQUEST);
            }
            if ($validate->fails()) {
                return response()->json([
                    'success' =>  false,
                    'message' => 'Gagal Lengkapi Data',
                    'data' => $validate->errors(),
                    'error' => 'error'
                ], Response::HTTP_BAD_REQUEST);
            }

            $data = [
                'category_id' => $request->category_id,
                'barcode' => $request->barcode,
                'name' => $request->name,
                'slug' => $request->name,
                'description' => $request->description,
                'price' => $request->price,
                'quantity' => $request->quantity,
            ];

            $check->fill($data);
            $check->save();

            if ($request->hasFile('image') && $request->image != '') {
                $img = $request->file('image');
                $imageName  = $img->hashName();
                $getUrl = storage_path('images') . '/' . $check->image->url;
                unlink($getUrl);
                $check->image->update([
                    'url' => $imageName
                ]);
                $img->move(storage_path('images'), $imageName);
            }
            return response()->json(new JsonResponse('Berhasil Edit data', $check), Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json(new JsonResponse(
                'Pembuatan Item Gagal',
                [
                    $e->getMessage(),
                ],
                'create_error'
            ), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function destroy(int $id)
    {
        try {
            $check = $this->items->find($id);
            if (!$check) {
                return response()->json([
                    'success' =>  false,
                    'message' => 'Id tidak ditemukan',
                    'data' =>  $check,
                    'error' => 'error'
                ], Response::HTTP_BAD_REQUEST);
            }
            if ($check->delete()) {
                $imgPath = storage_path('images') . '/' . $check->image->url;
                unlink($imgPath);
                $check->image->delete();
            }
            $check->delete();
            return response()->json([
                'success' =>  true,
                'message' => 'Sukses Deleted',
                'data' => [],
            ], Response::HTTP_BAD_REQUEST);
        } catch (\Exception $e) {
            return response()->json(new JsonResponse(
                'Hapus Item Gagal',
                [
                    $e->getMessage(),
                ],
                'deleted_error'
            ), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
