<?php

use Illuminate\Support\Str;
use App\Http\Controllers\TodoController;
use App\Http\Controllers\ControllerUser;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\TableController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ItemController;

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/key', function () use ($router) {
    return Str::random(32);
});

$router->group(['prefix' => 'todo'], function () use ($router) {
    $router->post('/todos', 'TodoController@store');
    $router->get('/todos', 'TodoController@index');
    $router->get('/todos/{id}', 'TodoController@shows');
    $router->put('/todos/{id}', 'TodoController@update');
    $router->delete('/todos/{id}', 'TodoController@destroy');
});


$router->group(['prefix' => 'api'], function () use ($router) {
    $router->post('auth/login', 'AuthController@login');
    #auth
    $router->group(['middleware' => 'auth'], function () use ($router) {
        $router->get('auth/me', 'AuthController@me');
        $router->get('auth/refresh', 'AuthController@refresh');
        $router->get('auth/logout', 'AuthController@logout');
    });

    $router->group(['middleware' => 'auth'], function () use ($router) {
        $router->get('categories', 'CategoryController@index');
        $router->post('categories', 'CategoryController@store');
        $router->get('categories/{id}', 'CategoryController@shows');
        $router->put('categories/{id}', 'CategoryController@update');
        $router->delete('categories/{id}', 'CategoryController@destroy');
    });

    $router->group(['middleware' => 'auth'], function () use ($router) {
        $router->get('table', 'TableController@index');
        $router->post('table', 'TableController@store');
        $router->get('table/{id}', 'TableController@shows');
        $router->put('table/{id}', 'TableController@update');
        $router->delete('table/{id}', 'TableController@destroy');
    });

    $router->group(['middleware' => 'auth'], function () use ($router) {
        $router->get('item', 'ItemController@index');
        $router->post('item', 'ItemController@create');
        $router->get('item/{id}', 'ItemController@shows');
        $router->put('item/{id}', 'ItemController@update');
        $router->delete('item/{id}', 'ItemController@destroy');
    });
});
